const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const webpush = require('web-push');
const Busboy = require('busboy');
const fs = require('fs');
const path = require('path');
const os = require('os');
const UUID = require('uuid-v4');

const serviceAccount = require('./pwagram_key.json');
const gcConfig = {
  projectId: 'pwagram-a01b5',
  keyFilename: 'pwagram_key.json'
};

const { Storage } = require('@google-cloud/storage');
const gcStorage = new Storage(gcConfig);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://pwagram-a01b5.firebaseio.com/'
});

exports.storePostData = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const uuid = UUID();
    const busboy = new Busboy({ headers: request.headers });
    // These objects will store the values (file + fields) extracted from busboy
    let upload;
    const fields = {};

    // This callback will be invoked for each file uploaded
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      console.log(
        `File [${fieldname}] filename: ${filename}, encoding: ${encoding}, mimetype: ${mimetype}`
      );
      const filepath = path.join(os.tmpdir(), filename);
      upload = { file: filepath, type: mimetype };
      file.pipe(fs.createWriteStream(filepath));
    });

    // This will invoked on every field detected
    busboy.on(
      'field',
      (
        fieldname,
        val,
        fieldnameTruncated,
        valTruncated,
        encoding,
        mimetype
      ) => {
        fields[fieldname] = val;
      }
    );

    // This callback will be invoked after all uploaded files are saved.
    busboy.on('finish', () => {
      var bucket = gcStorage.bucket('pwagram-a01b5.appspot.com');
      bucket.upload(
        upload.file,
        {
          uploadType: 'media',
          metadata: {
            metadata: {
              contentType: upload.type,
              firebaseStorageDownloadTokens: uuid
            }
          }
        },
        (ex, file) => {
          if (!ex) {
            admin
              .database()
              .ref('posts')
              .push({
                id: fields.id,
                title: fields.title,
                location: fields.location,
                rawLocation: {
                  lat: fields.rawLocationLat,
                  lng: fields.rawLocationLng
                },
                image: `https://firebasestorage.googleapis.com/v0/b/${
                  bucket.name
                }/o/${encodeURIComponent(file.name)}?alt=media&token=${uuid}`
              })
              .then(() => {
                webpush.setVapidDetails(
                  'mailto:business@alex.com',
                  'BNAeKY1mIxtp-gE521fZrmfkm_N4hzQ-70Qc3Rc7m_b9LBBuDV99CQ8WMLGqmecAfSe7iN1Hs1XyEk9nomxvb7A',
                  '-pML-rkAGcPauWk7B-P2YPb_WXQe7_UTYa1CPqmnDyw'
                );
                return admin.database().ref('subscriptions').once('value');
              })
              .then((subs) => {
                subs.forEach((sub) => {
                  const pushConfig = {
                    endpoint: sub.val().endpoint,
                    keys: {
                      auth: sub.val().keys.auth,
                      p256dh: sub.val().keys.p256dh
                    }
                  };

                  // eslint-disable-next-line promise/no-nesting
                  webpush
                    .sendNotification(
                      pushConfig,
                      JSON.stringify({
                        title: 'New Post',
                        content: 'New Post added',
                        openUrl: '/help'
                      })
                    )
                    .catch((ex) => {
                      console.log(ex);
                    });
                });
                return response
                  .status(201)
                  .json({ message: 'Data stored', id: fields.id });
              })
              .catch((err) => {
                return response.status(500).json({ error: err });
              });
          } else {
            console.log(ex);
          }
        }
      );
    });

    busboy.end(request.rawBody);
  });
});
