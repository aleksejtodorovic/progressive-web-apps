const shareImageButton = document.querySelector('#share-image-button');
const createPostArea = document.querySelector('#create-post');
const closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn');
const sharedMomentsArea = document.querySelector('#shared-moments');
const feedForm = document.querySelector('form'); // only form on a page
const titleInput = document.querySelector('#title');
const locationInput = document.querySelector('#location');
const videoPlayer = document.querySelector('#player');
const canvasElement = document.querySelector('#canvas');
const captureButton = document.querySelector('#capture-btn');
const imagePicker = document.querySelector('#image-picker');
const imagePickerContainer = document.querySelector('#pick-image');
const locationBtn = document.querySelector('#location-btn');
const locationLoader = document.querySelector('#location-loader');
let picture;
let fetchedLocation = { lat: 0, lng: 0 };
let sawAlert = false;

locationBtn.addEventListener('click', event => {
  if (!('geolocation' in navigator)) {
    return;
  }

  locationBtn.style.display = 'none';
  locationLoader.style.display = 'block';

  navigator.geolocation.getCurrentPosition(position => {
    locationBtn.style.display = 'inline';
    locationLoader.style.display = 'none';
    console.log(position);
    fetchedLocation = { lat: position.coords.latitude, lng: position.coords.longitude };
    locationInput.value = 'In Banja Luka'; // fetch from google api (or something else) city based on latitude and longitude;
    document.querySelector('#manual-location').classList.add('is-focused');
  }, error => {
    console.log(error);
    locationBtn.style.display = 'inline';
    locationLoader.style.display = 'none';
    if (!sawAlert) {
      sawAlert = true;
      alert('Couldn\'t fetch location, please enter manually!');
    }
    fetchedLocation = { lat: 0, lng: 0 };
  }, { timeout: 5000 });
});

initializeLoxaction = () => {
  if (!('geolocation' in navigator)) {
    locationBtn.style.display = 'none';
  }
}

initializeMedia = () => {
  if (!('mediaDevices' in navigator)) {
    navigator.mediaDevices = {};
  }

  if (!('getUserMedia' in navigator.mediaDevices)) {
    navigator.mediaDevices.getUserMedia = constraints => {
      const getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUerMedia;

      if (!getUserMedia) {
        return Promise.reject(new Error('getUserMedia is not implemented'));
      }

      return new Promise((resolve, reject) => {
        getUserMedia.call(navigator, constraints, resolve, reject);
      });
    }
  }

  navigator.mediaDevices.getUserMedia({ audio: false, video: true })
    .then(stream => {
      videoPlayer.srcObject = stream;
      videoPlayer.style.display = 'block';
      captureButton.style.display = 'block';
    })
    .catch(ex => {
      imagePickerContainer.style.display = 'block';
    });
}

closeCapturingVideo = () => {
  videoPlayer.srcObject.getVideoTracks().forEach(track => {
    track.stop();
  });
}

captureButton.addEventListener('click', event => {
  canvasElement.style.display = 'block';
  videoPlayer.style.display = 'none';
  captureButton.style.display = 'none';

  const context = canvasElement.getContext('2d');
  context.drawImage(videoPlayer, 0, 0, canvasElement.width, videoPlayer.videoHeight / (videoPlayer.videoWidth / canvas.width));
  closeCapturingVideo();
  picture = dataURItoBlob(canvasElement.toDataURL());
});

imagePicker.addEventListener('change', event => {
  picture = event.target.files[0];
});

openCreatePostModal = () => {
  setTimeout(() => {
    createPostArea.style.transform = 'translateY(0)';
  }, 1);
  initializeMedia();
  initializeLoxaction();
  // if (defferedPrompt) {
  //   defferedPrompt.prompt();

  //   defferedPrompt.userChoice
  //     .then(choiceResult => {
  //       if (choiceResult.outcome === 'dismissed') {
  //         console.log('User canceled installation');
  //       } else {
  //         console.log('User added to home screen');
  //       }
  //     });

  //   defferedPrompt = null;
  // }
}

// if ('serviceWorker' in navigator) {
//   navigator.serviceWorker.getRegistrations()
//     .then(registrations => {
//       registrations.forEach(registration => registration.unregister());
//     });
// }

closeCreatePostModal = () => {
  createPostArea.style.transform = 'translateY(100vh)';
  imagePickerContainer.style.display = 'none';
  videoPlayer.style.display = 'none';
  canvasElement.style.display = 'none';
  locationBtn.style.display = 'inline';
  locationLoader.style.display = 'none';
  closeCapturingVideo();
}

shareImageButton.addEventListener('click', openCreatePostModal);

closeCreatePostModalButton.addEventListener('click', closeCreatePostModal);


// Currently not in use 
// onSaveButtonClick = evt => {
//   console.log('Clicked on save');
//   if ('caches' in window) {
//     caches.open('user-requested')
//       .then(cache => {
//         cache.add('https://httpbin.org/get');
//         cache.add('/src/images/sf-boat.jpg');
//       });
//   }
// }

clearCars = data => {
  while (sharedMomentsArea.hasChildNodes()) {
    sharedMomentsArea.removeChild(sharedMomentsArea.lastChild);
  }
}

createCard = ({ image, title, location }) => {
  const cardWrapper = document.createElement('div');
  cardWrapper.className = 'shared-moment-card mdl-card mdl-shadow--2dp';

  const cardTitle = document.createElement('div');
  cardTitle.className = 'mdl-card__title';
  cardTitle.style.backgroundImage = `url(${image})`;
  cardTitle.style.backgroundSize = 'cover';
  cardTitle.style.height = '180px';
  cardWrapper.appendChild(cardTitle);

  const cardTitleTextElement = document.createElement('h2');
  cardTitleTextElement.style.color = 'white';
  cardTitleTextElement.className = 'mdl-card__title-text';
  cardTitleTextElement.textContent = title;
  cardTitle.appendChild(cardTitleTextElement);

  const cardSupportingText = document.createElement('div');
  cardSupportingText.className = 'mdl-card__supporting-text';
  cardSupportingText.textContent = location;
  cardSupportingText.style.textAlign = 'center';
  // const cardSaveButton = document.createElement('button');
  // cardSaveButton.textContent = 'Save';
  // cardSupportingText.appendChild(cardSaveButton);
  // cardSaveButton.addEventListener('click', onSaveButtonClick);
  cardWrapper.appendChild(cardSupportingText);
  componentHandler.upgradeElement(cardWrapper);
  sharedMomentsArea.appendChild(cardWrapper);
}

updateUI = data => {
  data.forEach(element => {
    createCard(element);
  });
}

const url = 'https://pwagram-a01b5.firebaseio.com/posts.json';
let networkDataReceived = false;

fetch(url)
  .then(response => {
    return response.json();
  })
  .then(data => {
    networkDataReceived = true;
    console.log('Response from web', data);
    clearCars();
    updateUI(Object.values(data));
  })
  .catch(ex => {
    console.log(ex);
  });

if ('indexedDB' in window) {
  readAllData('posts')
    .then(posts => {
      if (!networkDataReceived) {
        console.log('From indexedDB ...', posts);
        updateUI(posts);
      }
    });
}

sendData = post => {
  const postData = new FormData();

  postData.append('id', post.id);
  postData.append('title', post.title);
  postData.append('location', post.location);
  postData.append('rawLocationLat', post.rawLocation.lat);
  postData.append('rawLocationLng', post.rawLocation.lng);
  postData.append('file', post.picture, post.id + '.png');

  fetch('https://us-central1-pwagram-a01b5.cloudfunctions.net/storePostData', {
    method: 'POST',
    headers: {
      'Accept': 'application/json'
    },
    body: postData
  })
    .then(res => {
      console.log('sent data', res);
      updateUI();
    })
}

feedForm.addEventListener('submit', event => {
  event.preventDefault();

  if (titleInput.value.trim() === '' || locationInput.value.trim() === '') {
    alert('Please enter valid data!');
    return;
  }

  closeCreatePostModal();

  const post = {
    title: titleInput.value,
    location: locationInput.value,
    id: new Date().toISOString(),
    picture: picture,
    rawLocation: fetchedLocation
  };

  if ('serviceWorker' in navigator && 'SyncManager' in window) {
    navigator.serviceWorker.ready
      .then(sw => {
        writeData('sync-posts', post)
          .then(() => {
            sw.sync.register('sync-new-post');
          })
          .then(() => {
            const snackbarContainer = document.querySelector('#confirmation-toast');
            const data = { message: 'Your Post was saved for syncing!' };

            snackbarContainer.MaterialSnackbar.showSnackbar(data);
          })
          .catch(ex => {
            console.log(ex);
          });
      });
  } else {
    sendData(post);
  }
});