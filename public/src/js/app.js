const enableNotificationsButtons = document.querySelectorAll(
  '.enable-notifications'
);
let defferedPrompt;

if (!window.Promise) {
  window.Promise = Promise;
}

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw.js')
    .then(() => {
      console.log('service worker registerd');
    })
    .catch((ex) => {
      console.log(ex);
    });
}

window.addEventListener('beforeinstallprompt', (event) => {
  console.log('beforeinstallprompt fired');
  event.preventDefault();
  defferedPrompt = event;

  return false;
});

displayConfirmNotification = () => {
  if ('serviceWorker' in navigator) {
    const options = {
      body: 'You have successfully subscribed to our Notification service.',
      icon: '/src/images/icons/app-icon-96x96.png',
      // image: '/src/images/sf-boat.jpg',
      dir: 'ltr',
      lang: 'en-US',
      vibrate: [100, 50, 200],
      badge: '/src/images/icons/app-icon-96x96.png',
      tag: 'confirm-notification', // they will stack if we send more of these notifications
      renotify: true,
      actions: [
        {
          action: 'confirm',
          title: 'Okay',
          icon: '/src/images/icons/app-icon-96x96.png'
        },
        {
          action: 'cancel',
          title: 'Cancel'
        }
      ]
    };

    navigator.serviceWorker.ready.then((sw) => {
      sw.showNotification('Successfully subscribed!', options);
    });
  }
};

configurePushSub = () => {
  if (!('serviceWorker' in navigator)) {
    return;
  }

  navigator.serviceWorker.ready.then((sw) => {
    sw.pushManager
      .getSubscription()
      .then((sub) => {
        if (sub === null) {
          const vapidPublicKey =
            'BNAeKY1mIxtp-gE521fZrmfkm_N4hzQ-70Qc3Rc7m_b9LBBuDV99CQ8WMLGqmecAfSe7iN1Hs1XyEk9nomxvb7A';
          const convertedVapidPublicKey = urlBase64ToUint8Array(vapidPublicKey);
          // Create new one
          return sw.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: convertedVapidPublicKey
          });
        } else {
          // Use existing subscription
        }
      })
      .then((newSub) => {
        return fetch(
          'https://pwagram-a01b5.firebaseio.com/subscriptions.json',
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json'
            },
            body: JSON.stringify(newSub)
          }
        );
      })
      .then((res) => {
        if (res.ok) {
          displayConfirmNotification();
        }
      })
      .catch((ex) => {
        console.log(ex);
      });
  });
};

askForNotificationPermission = (evt) => {
  Notification.requestPermission((result) => {
    console.log('User choice', result);

    if (result !== 'granted') {
      console.log('No notification permission granted!');
    } else {
      // hide button
      // displayConfirmNotification();
      configurePushSub();
    }
  });
};

if ('Notification' in window && 'serviceWorker' in navigator) {
  enableNotificationsButtons.forEach((button) => {
    button.style.display = 'inline-block';
    button.addEventListener('click', askForNotificationPermission);
  });
}
