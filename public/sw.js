importScripts('/src/js/idb.js', '/src/js/indexedDB.js', '/src/js/utility.js');

const CACHE_STATIC_NAME = 'static-v11';
const CACHE_DYNAMIC_NAME = 'dynamic-v2';
const STATIC_FILES = [
    '/',
    '/index.html',
    '/offline.html',
    '/src/js/app.js',
    '/src/js/idb.js',
    '/src/js/indexedDB.js',
    '/src/js/utility.js',
    '/src/js/feed.js',
    '/src/js/promise.js', // for performance reason, this one is loaded for old browsers to support promise. those browsers do not use service workers
    '/src/js/fetch.js',
    '/src/js/material.min.js',
    '/src/css/app.css',
    '/src/css/feed.css',
    '/src/images/main-image.jpg',
    'https://fonts.googleapis.com/css?family=Roboto:400,700',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css'
];

// trimCache = (cacheName, maxItems) => {
//     caches.open(cacheName)
//         .then(cache => {
//             return cache.keys()
//                 .then(keys => {
//                     if (keys.length > maxItems) {
//                         cache.delete(keys[0])
//                             .then(trimCache(cacheName, maxItems));
//                     }
//                 });
//         })
// }

self.addEventListener('install', event => {
    console.log('[Service Worker] Installing service worker ...', event);
    event.waitUntil( // wait for preparation before it continious. Fetch comes, or something else ...
        caches.open(CACHE_STATIC_NAME)
            .then(cache => {
                console.log('[Service Worker] Precaching App Chell');
                cache.addAll(STATIC_FILES);
            })

    );
});

self.addEventListener('activate', event => {
    console.log('[Service Worker] Activating service worker ...', event);
    event.waitUntil(
        caches.keys()
            .then(cacheKeys => {
                return Promise.all(cacheKeys.map(key => {
                    if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
                        console.log('[Service Worker] Removing old cache...', key);
                        return caches.delete(key);
                    }
                }));
            })
    )
    return self.clients.claim();
});

isInArray = (url, array) => {
    let cachePath;

    if (url.indexOf(self.origin) === 0) { // request targets domain where we serve the page from (i.e. NOT a CDN)
        console.log('matched ', url);
        cachePath = url.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
    } else {
        cachePath = url; // store the full request (for CDNs)
    }
    return array.indexOf(cachePath) > -1;
}

self.addEventListener('fetch', event => {
    const url = 'https://pwagram-a01b5.firebaseio.com/posts.json';

    // Cache, then Network Strategy
    // Apply this strategy only for this url (json request), otherwise use cache with network fallback
    if (event.request.url.indexOf(url) > -1) {
        event.respondWith(
            fetch(event.request)
                .then(res => {
                    const clonedResponse = res.clone();
                    clearAllData('posts')
                        .then(() => {
                            return clonedResponse.json();
                        })
                        .then(data => {
                            Object.values(data).forEach(post => {
                                writeData('posts', post);
                            });
                        })
                        .catch(ex => {
                            console.log(ex);
                        });

                    return res;
                })
        );
    } else if (isInArray(event.request.url, STATIC_FILES)) {
        // Static files returned only from cache. It is possible because they are precached when service worker is installed.
        event.respondWith(
            caches.match(event.request)
        );
    } else {
        // Cache with network fallback strategy.
        event.respondWith(
            caches.match(event.request)
                .then(response => {
                    if (response) {
                        return response;
                    } else {
                        return fetch(event.request)
                            .then(serverResponse => {
                                return caches.open(CACHE_DYNAMIC_NAME)
                                    .then(cache => {
                                        // trimCache(CACHE_DYNAMIC_NAME, 3); // clean cache before adding new item
                                        cache.put(event.request.url, serverResponse.clone()); // response can be used only once, so that is why clone is used
                                        return serverResponse;
                                    });
                            })
                            .catch(() => {
                                return caches.open(CACHE_STATIC_NAME)
                                    .then(cache => {
                                        if (event.request.headers.get('accept').includes('text/html')) { // return only when fetching html, it does not make sence for .css file lets say
                                            return cache.match('/offline.html');
                                        }
                                    });
                            });
                    }
                })
        );
    }
});

self.addEventListener('sync', event => {
    console.log('[Service Worker] Background syncing', event);

    if (event.tag === 'sync-new-post') {
        console.log('[Service Worker] Syncing new Post');
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    const url = 'https://us-central1-pwagram-a01b5.cloudfunctions.net/storePostData';

                    data.forEach(post => {
                        const postData = new FormData();
                        postData.append('id', post.id);
                        postData.append('title', post.title);
                        postData.append('location', post.location);
                        postData.append('rawLocationLat', post.rawLocation.lat);
                        postData.append('rawLocationLng', post.rawLocation.lng);
                        postData.append('file', post.picture, post.id + '.png');

                        fetch(url, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json'
                            },
                            body: postData
                        })
                            .then(res => {
                                console.log('sent data', res);
                                if (res.ok) {
                                    deleteItemFromData('sync-posts', res.id);
                                }
                            })
                            .catch(ex => {
                                console.log('error while syncing posts', ex);
                            })
                    })
                })
        );
    }
});

self.addEventListener('notificationclick', event => {
    const notification = event.notification;

    if (event.action === 'confirm') {
        console.log('Confirm was chosen', notification);
    } else {
        console.log(event.action);
        event.waitUntil(
            clients.matchAll()
                .then(clis => {
                    const client = clis.find(c => c.visibilityState === 'visible');

                    if (client) {
                        client.navigate(notification.data.url);
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
    }

    notification.close();
});

self.addEventListener('notificationclose', event => {
    console.log('Notification was closed', event);
});

self.addEventListener('push', event => {
    console.log('Push Notification received', event);

    let data = {
        title: 'New!',
        content: 'Something new happend!',
        openUrl: '/'
    };

    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    const options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        badge: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        }
    };

    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});