importScripts('workbox-sw.prod.v2.1.3.js');
importScripts('/src/js/idb.js', '/src/js/indexedDB.js', '/src/js/utility.js');

const workboxSW = new self.WorkboxSW();

workboxSW.router.registerRoute(/.*(?:googleapis|gstatic)\.com.*$/, workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'google-fonts',
    cacheExpiration: {
        maxEntries: 3,
        maxAgeSeconds: 60 * 60 * 24 * 30 // every month
    }
})); // cache then netowrk

workboxSW.router.registerRoute('https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css', workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'material-css'
}));

workboxSW.router.registerRoute('https://pwagram-a01b5.firebaseio.com/posts.json', ({ event }) => {
    return fetch(event.request)
        .then(res => {
            const clonedResponse = res.clone();
            clearAllData('posts')
                .then(() => {
                    return clonedResponse.json();
                })
                .then(data => {
                    Object.values(data).forEach(post => {
                        writeData('posts', post);
                    });
                })
                .catch(ex => {
                    console.log(ex);
                });

            return res;
        })
});

workboxSW.router.registerRoute(routeData => {
    return (routeData.event.request.headers.get('accept').includes('text/html'));
}, ({ event }) => {
    return caches.match(event.request)
        .then(response => {
            if (response) {
                return response;
            } else {
                return fetch(event.request)
                    .then(serverResponse => {
                        return caches.open('dynamic')
                            .then(cache => {
                                // trimCache(CACHE_DYNAMIC_NAME, 3); // clean cache before adding new item
                                cache.put(event.request.url, serverResponse.clone()); // response can be used only once, so that is why clone is used
                                return serverResponse;
                            });
                    })
                    .catch(() => {
                        console.log('from cache');
                        return caches.match('/offline.html')
                            .then(res => {
                                console.log('res found', res);
                                return res;
                            });
                    });
            }
        })
});

workboxSW.router.registerRoute(/.*(?:firebasestorage\.googleapis)\.com.*&/, workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'post-images'
}));

workboxSW.precache([]);

self.addEventListener('sync', event => {
    console.log('[Service Worker] Background syncing', event);

    if (event.tag === 'sync-new-post') {
        console.log('[Service Worker] Syncing new Post');
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    const url = 'https://us-central1-pwagram-a01b5.cloudfunctions.net/storePostData';

                    data.forEach(post => {
                        const postData = new FormData();
                        postData.append('id', post.id);
                        postData.append('title', post.title);
                        postData.append('location', post.location);
                        postData.append('rawLocationLat', post.rawLocation.lat);
                        postData.append('rawLocationLng', post.rawLocation.lng);
                        postData.append('file', post.picture, post.id + '.png');

                        fetch(url, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json'
                            },
                            body: postData
                        })
                            .then(res => {
                                console.log('sent data', res);
                                if (res.ok) {
                                    deleteItemFromData('sync-posts', res.id);
                                }
                            })
                            .catch(ex => {
                                console.log('error while syncing posts', ex);
                            })
                    })
                })
        );
    }
});

self.addEventListener('notificationclick', event => {
    const notification = event.notification;

    if (event.action === 'confirm') {
        console.log('Confirm was chosen', notification);
    } else {
        console.log(event.action);
        event.waitUntil(
            clients.matchAll()
                .then(clis => {
                    const client = clis.find(c => c.visibilityState === 'visible');

                    if (client) {
                        client.navigate(notification.data.url);
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
    }

    notification.close();
});

self.addEventListener('notificationclose', event => {
    console.log('Notification was closed', event);
});

self.addEventListener('push', event => {
    console.log('Push Notification received', event);

    let data = {
        title: 'New!',
        content: 'Something new happend!',
        openUrl: '/'
    };

    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    const options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        badge: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        }
    };

    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});