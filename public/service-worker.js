importScripts('workbox-sw.prod.v2.1.3.js');
importScripts('/src/js/idb.js', '/src/js/indexedDB.js', '/src/js/utility.js');

const workboxSW = new self.WorkboxSW();

workboxSW.router.registerRoute(/.*(?:googleapis|gstatic)\.com.*$/, workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'google-fonts',
    cacheExpiration: {
        maxEntries: 3,
        maxAgeSeconds: 60 * 60 * 24 * 30 // every month
    }
})); // cache then netowrk

workboxSW.router.registerRoute('https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css', workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'material-css'
}));

workboxSW.router.registerRoute('https://pwagram-a01b5.firebaseio.com/posts.json', ({ event }) => {
    return fetch(event.request)
        .then(res => {
            const clonedResponse = res.clone();
            clearAllData('posts')
                .then(() => {
                    return clonedResponse.json();
                })
                .then(data => {
                    Object.values(data).forEach(post => {
                        writeData('posts', post);
                    });
                })
                .catch(ex => {
                    console.log(ex);
                });

            return res;
        })
});

workboxSW.router.registerRoute(routeData => {
    return (routeData.event.request.headers.get('accept').includes('text/html'));
}, ({ event }) => {
    return caches.match(event.request)
        .then(response => {
            if (response) {
                return response;
            } else {
                return fetch(event.request)
                    .then(serverResponse => {
                        return caches.open('dynamic')
                            .then(cache => {
                                // trimCache(CACHE_DYNAMIC_NAME, 3); // clean cache before adding new item
                                cache.put(event.request.url, serverResponse.clone()); // response can be used only once, so that is why clone is used
                                return serverResponse;
                            });
                    })
                    .catch(() => {
                        console.log('from cache');
                        return caches.match('/offline.html')
                            .then(res => {
                                console.log('res found', res);
                                return res;
                            });
                    });
            }
        })
});

workboxSW.router.registerRoute(/.*(?:firebasestorage\.googleapis)\.com.*&/, workboxSW.strategies.staleWhileRevalidate({
    cacheName: 'post-images'
}));

workboxSW.precache([
  {
    "url": "favicon.ico",
    "revision": "2cab47d9e04d664d93c8d91aec59e812"
  },
  {
    "url": "help/index.html",
    "revision": "b656ccb86249a2f286c81442a78be420"
  },
  {
    "url": "index.html",
    "revision": "42d2bc42ff0a276ff42fdf582e2b34a8"
  },
  {
    "url": "manifest.json",
    "revision": "d11c7965f5cfba711c8e74afa6c703d7"
  },
  {
    "url": "offline.html",
    "revision": "3bdf3b700444af6e8f07a813b6d0f380"
  },
  {
    "url": "src/css/app.css",
    "revision": "f27b4d5a6a99f7b6ed6d06f6583b73fa"
  },
  {
    "url": "src/css/feed.css",
    "revision": "a56fe900cf770f1f71d6c889d66cec5e"
  },
  {
    "url": "src/css/help.css",
    "revision": "1c6d81b27c9d423bece9869b07a7bd73"
  },
  {
    "url": "src/images/main-image-lg.jpg",
    "revision": "31b19bffae4ea13ca0f2178ddb639403"
  },
  {
    "url": "src/images/main-image-sm.jpg",
    "revision": "c6bb733c2f39c60e3c139f814d2d14bb"
  },
  {
    "url": "src/images/main-image.jpg",
    "revision": "5c66d091b0dc200e8e89e56c589821fb"
  },
  {
    "url": "src/images/sf-boat.jpg",
    "revision": "0f282d64b0fb306daf12050e812d6a19"
  },
  {
    "url": "src/js/app.min.js",
    "revision": "130a3ad41b80ef79c91d57bd51441865"
  },
  {
    "url": "src/js/feed.min.js",
    "revision": "8741b790d4cb770c92dbd23b20575642"
  },
  {
    "url": "src/js/fetch.min.js",
    "revision": "7dd12e47099030e151d85b3cd101df5c"
  },
  {
    "url": "src/js/idb.min.js",
    "revision": "636063f0d15e3e64ee8311e5e7618ae8"
  },
  {
    "url": "src/js/indexedDB.min.js",
    "revision": "339b363b3c72a9f6394f1d2aa800c4c7"
  },
  {
    "url": "src/js/material.min.js",
    "revision": "d0708cc15f84dd15e577bd4348b9781d"
  },
  {
    "url": "src/js/promise.min.js",
    "revision": "95dabba24f672f75cef1c3de0d0b9253"
  },
  {
    "url": "src/js/utility.min.js",
    "revision": "cc6c5c946790de79eec285743c127f96"
  }
]);

self.addEventListener('sync', event => {
    console.log('[Service Worker] Background syncing', event);

    if (event.tag === 'sync-new-post') {
        console.log('[Service Worker] Syncing new Post');
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    const url = 'https://us-central1-pwagram-a01b5.cloudfunctions.net/storePostData';

                    data.forEach(post => {
                        const postData = new FormData();
                        postData.append('id', post.id);
                        postData.append('title', post.title);
                        postData.append('location', post.location);
                        postData.append('rawLocationLat', post.rawLocation.lat);
                        postData.append('rawLocationLng', post.rawLocation.lng);
                        postData.append('file', post.picture, post.id + '.png');

                        fetch(url, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json'
                            },
                            body: postData
                        })
                            .then(res => {
                                console.log('sent data', res);
                                if (res.ok) {
                                    deleteItemFromData('sync-posts', res.id);
                                }
                            })
                            .catch(ex => {
                                console.log('error while syncing posts', ex);
                            })
                    })
                })
        );
    }
});

self.addEventListener('notificationclick', event => {
    const notification = event.notification;

    if (event.action === 'confirm') {
        console.log('Confirm was chosen', notification);
    } else {
        console.log(event.action);
        event.waitUntil(
            clients.matchAll()
                .then(clis => {
                    const client = clis.find(c => c.visibilityState === 'visible');

                    if (client) {
                        client.navigate(notification.data.url);
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
    }

    notification.close();
});

self.addEventListener('notificationclose', event => {
    console.log('Notification was closed', event);
});

self.addEventListener('push', event => {
    console.log('Push Notification received', event);

    let data = {
        title: 'New!',
        content: 'Something new happend!',
        openUrl: '/'
    };

    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    const options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        badge: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        }
    };

    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});